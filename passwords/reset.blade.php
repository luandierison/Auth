@extends('layout')

@section('conteudo')
<head>
  <style>
    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
    }

    main {
      flex: 1 0 auto;
    }

    body {
      background: #fff;
    }

    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
      color: #b71c1c;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
      border-bottom: 2px solid #b71c1c;
      box-shadow: none;
    }
  </style>
</head>

<body>
  <div class="section"></div>
  <main>
    <center>
      <img class="responsive-img" style="width: 250px;" src="http://i.imgur.com/ax0NCsK.gif" />
      <div class="section"></div>

      <h5 class="indigo-text">Redefinir Senha</h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 40px 0px 40px; border: 1px solid #EEE;">

          <form class="form-horizontal" role="form" method="post" action="{{ url('/password/reset') }}">
          {{ csrf_field() }}
          <input type="hidden" name="token" value="{{ $token }}">
            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

            <div class='row'>
              <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                <input class='validate' type='email' name='email' id='email' value="{{ $email or old('email') }}" required/>
                <label for='email'>Digite seu email</label>
                 @if ($errors->has('email'))
                        <div class="col s12">
                            <span class="red-text">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        </div>
                    @endif
              </div>
            </div>

            <div class='row'>
              <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}" required">
                <input class='validate' type='password' name='password' id='password' min="8"/>
                <label for='password'>Digite sua senha</label>
                @if ($errors->has('password'))
                        <span class="red-text">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
              </div>
            </div>

            <div class='row'>
                <div class="input-field col s12{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password-confirm" type="password" class="validate" name="password_confirmation">
                <label for="password-confirm">Confirme sua senha</label>
                @if ($errors->has('password_confirmation'))
                    <span class="red-text">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
                </div>
            </div>


            <br />
            <center>
              <div class='row'>
                <button type="submit" class='btn btn-large waves-effect grey darken-4'>Resetar Senha</button>
              </div>
            </center>
          </form>
        </div>
      </div>
    </center>

  </main>

</body>
@endsection
