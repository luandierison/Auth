@extends('layout')

<!-- Main Content -->
@section('conteudo')
<head>
  <style>
    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
    }

    main {
      flex: 1 0 auto;
    }

    body {
      background: #fff;
    }

    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
      color: #b71c1c;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
      border-bottom: 2px solid #b71c1c;
      box-shadow: none;
    }
  </style>
</head>

<body>
  <div class="section"></div>
  <main>
    <center>
      <img class="responsive-img" style="width: 250px;" src="http://i.imgur.com/ax0NCsK.gif" />
      <div class="section"></div>

      <h5 class="indigo-text">Redifinir Senha</h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

          <form role="form" class="form-horizontal" method="post" action="{{ url('/password/email') }}">
          {{ csrf_field() }}

            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

            <div class='row'>
            <div class="input-field col s12{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" name="email" id="email" value="{{ old('email') }}" class="validate">
                <label for="email">Digite seu e-mail</label>
                @if ($errors->has('email'))
                    <span class="red-text">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            </div>

            <center>
              <div class='row'>
                <button type='submit' class='col s12 btn btn-large waves-effect grey darken-4'>Enviar Link de Recuperação</button>
              </div>
            </center>
          </form>
        </div>
      </div>
    </center>

    
  </main>

</body>
@endsection
