@extends('layout')

@section('conteudo')
<head>
  <style>
    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
    }

    main {
      flex: 1 0 auto;
    }

    body {
      background: #fff;
    }

    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
      color: #b71c1c;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
      border-bottom: 2px solid #b71c1c;
      box-shadow: none;
    }
  </style>
</head>

<body>
  <div class="section"></div>
  <main>
    <center>
      <img class="responsive-img" style="width: 250px;" src="http://i.imgur.com/ax0NCsK.gif" />
      <div class="section"></div>

      <h5 class="indigo-text">Login</h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 40px 0px 40px; border: 1px solid #EEE;">

          <form role="form" class="col s12" method="post" action="{{ url('/login') }}">
          {{ csrf_field() }}

            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

            <div class='row'>
              <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                <input class='validate' type='email' name='email' id='email' value="{{ old('email') }}" required/>
                <label for='email'>Digite seu email</label>
                 @if ($errors->has('email'))
                        <div class="col s12">
                            <span class="red-text">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        </div>
                    @endif
              </div>
            </div>

            <div class='row'>
              <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}" required">
                <input class='validate' type='password' name='password' id='password' min="8"/>
                <label for='password'>Digite sua senha</label>
                @if ($errors->has('password'))
                        <span class="red-text">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
              </div>
              <label style='float: right;'>
                 <a href="{{ url('/password/reset') }}"><b>Esqueceu sua senha?</b></a>
              </label>
              <label style='float: left;'>
                  <input type="checkbox" id="remember" name="remember" />
                  <label for="remember">Lembrar</label>
              </label>
            </div>

            <br />
            <center>
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect grey darken-4'>Login</button>
              </div>
            </center>
          </form>
        </div>
      </div>
      <a href="{{ route('register') }}">Crie uma conta</a>
    </center>

  </main>

</body>
@endsection
