@extends('layout')

@section('conteudo')
<head>
  <style>
    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
    }

    main {
      flex: 1 0 auto;
    }

    body {
      background: #fff;
    }

    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
      color: #b71c1c;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
      border-bottom: 2px solid #b71c1c;
      box-shadow: none;
    }
  </style>
</head>

<body>
  <div class="section"></div>
  <main>
    <center>
      <img class="responsive-img" style="width: 200px;" src="http://i.imgur.com/ax0NCsK.gif" />
      <div class="section"></div>

      <h5 class="indigo-text">Registre-se</h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 40px 0px 40px; border: 1px solid #EEE;">

          <form role="form" class="col s12" method="post" action="{{ url('/register') }}">
          {{ csrf_field() }}

           
            <div class='row'>
              <div class="input-field col s12 {{ $errors->has('name') ? ' has-error' : '' }}">
                <input id="name" type="text" class="validate" name="name" value="{{ old('name') }}" required/>
                 <label for="name">Digite seu nome</label>
                 @if ($errors->has('name'))
                        <span class="red-text">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                @endif
              </div>
            </div>

            <div class='row'>
            <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                  <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required/>
                  <label for="email">Digite um email</label>
                    @if ($errors->has('email'))
                        <div class="col s12">
                            <span class="red-text">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        </div>
                    @endif
            </div>
            </div>

            <div class='row'>
            <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}" required>
                    <input type="password" name="password" class="validate" min="8" id="password"/>
                    <label for="password">Senha</label>
                    @if ($errors->has('password'))
                        <span class="red-text">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>
            </div>
            
            <div class='row'>
            <div class="input-field col s12 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input id="password-confirm" type="password" class="validate" name="password_confirmation"/>
                    <label>Confirmar senha</label>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
            </div>
            </div>
            
            <center>
              <div class='row'>
                <button type='submit' class='col s12 btn btn-large waves-effect grey darken-4'>Registrar</button>
              </div>
            </center>

          </form>
        </div>
      </div>
    </center>

  </main>

</body>
@endsection
